import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TerrorService {

  constructor(
    private http: HttpClient
  ) { }

  buscarFilmesTerror(): Observable<any> {
    return this.http.get<any>(`https://api.themoviedb.org/3/discover/movie?api_key=8e6b29ed77341eb6435f2b2ca6bcce6f&with_genres=27`)
  }
}
