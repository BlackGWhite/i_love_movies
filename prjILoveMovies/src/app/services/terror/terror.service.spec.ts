import { TestBed } from '@angular/core/testing';

import { TerrorService } from './terror.service';

describe('TerrorService', () => {
  let service: TerrorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TerrorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
