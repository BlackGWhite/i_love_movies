import { TestBed } from '@angular/core/testing';

import { FantasiaService } from './fantasia.service';

describe('FantasiaService', () => {
  let service: FantasiaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FantasiaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
