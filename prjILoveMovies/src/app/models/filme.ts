export class filme{
    poster_path: string;
    adult: boolean;
    overview: string;
    release_date: string;
    genre_ids: number[];
    id: number;
    original_title: string;
    original_language: string;
    title: string;
    backdrop_path: string;
    popularity: number;
    vote_count: number;
    video: boolean;
    vote_average: number;

        constructor(){
            this.poster_path = '';
            this.adult = false;
            this.overview = '';
            this.release_date = '';
            this.genre_ids = [];
            this.id = 0;
            this.original_title = '';
            this.original_language = '';
            this.title = '';
            this.backdrop_path = '';
            this.popularity = 0;
            this.vote_count = 0;
            this.video = false;
            this.vote_average = 0;
        }

}