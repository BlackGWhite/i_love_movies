import { Component, OnInit } from '@angular/core';
import { filme } from 'src/app/models/filme';

import { FantasiaService } from '../services/fantasia/fantasia.service';

@Component({
  selector: 'app-tab3',
  templateUrl: './tab3.page.html',
  styleUrls: ['./tab3.page.scss'],
})
export class Tab3Page implements OnInit {
  filmes: filme[];

  constructor(
    private FantasiaService: FantasiaService
  ) {
    this.filmes = [];
  }

  buscarFilmesFantasia = () => {
    this.FantasiaService.buscarFilmesFantasia().subscribe((data) => {
      this.filmes = data.results;
    });
  }

  ngOnInit() {
    this.buscarFilmesFantasia();
  }
}

