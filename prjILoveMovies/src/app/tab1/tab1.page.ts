import { Component, OnInit } from '@angular/core';
import { filme } from 'src/app/models/filme';

import { RomanceService } from '../services/romance/romance.service';

@Component({
  selector: 'app-tab1',
  templateUrl: './tab1.page.html',
  styleUrls: ['./tab1.page.scss'],
})
export class Tab1Page implements OnInit {
  filmes: filme[];

  constructor(
    private RomanceService: RomanceService
  ) {
    this.filmes = [];
  }

  buscarFilmesRomance = () => {
    this.RomanceService.buscarFilmesRomance().subscribe((data) => {
      this.filmes = data.results;
    });
  }

  ngOnInit() {
    this.buscarFilmesRomance();
  }
}