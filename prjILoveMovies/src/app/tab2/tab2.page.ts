import { Component, OnInit } from '@angular/core';
import { filme } from 'src/app/models/filme';

import { TerrorService } from '../services/terror/terror.service';

@Component({
  selector: 'app-tab2',
  templateUrl: './tab2.page.html',
  styleUrls: ['./tab2.page.scss'],
})
export class Tab2Page implements OnInit {
  filmes: filme[];

  constructor(
    private TerrorService: TerrorService
  ) {
    this.filmes = [];
  }

  buscarFilmesTerror = () => {
    this.TerrorService.buscarFilmesTerror().subscribe((data) => {
      this.filmes = data.results;
    });
  }

  ngOnInit() {
    this.buscarFilmesTerror();
  }
}
